package com.example.tp11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Animallist aa=new Animallist();
        final ListView MyListView;
        MyListView = findViewById(R.id.MyListView);
        String[] val =aa.getNameArray();
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,val);
MyListView.setAdapter(adapter);
MyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final String item=(String) parent.getItemAtPosition(position);

        Intent intent = new Intent(MainActivity.this,animal1.class);
        intent.putExtra("nom",item);
        startActivity(intent);

    }
});
    }
}
